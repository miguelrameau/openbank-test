import React from "react";
import i18next from "i18next";

import "../Step1/index.scss";
import savePassword from "./save-password.png";
import createMasterKey from "./create-master-key.png";
const Step1 = (props) => {
  return (
    <div className="step1-comp">
      <ul className="step1-comp__info">
        <li>
          <img src={savePassword} />
          <p>{i18next.t("info_save_password")}</p>
        </li>
        <li>
          <img src={createMasterKey} />
          <p>{i18next.t("info_create_master_key")}</p>
        </li>
      </ul>
      <p>
        <h3>{i18next.t("title_how_it_works")}</h3>
        {i18next.t("text_how_it_works")}
      </p>
      <p>
        <h3>{i18next.t("title_which_data_to_save")}</h3>
        {i18next.t("text_which_data_to_save")}
      </p>
    </div>
  );
};

export default Step1;
