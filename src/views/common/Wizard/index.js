import React, { Children, isValidElement, cloneElement } from "react";
import i18next from "i18next";
import Popup from "../Popup";
import WizardHeader from "./WizardHeader";
import "./index.scss";

const Wizard = (props) => {
  // const [stepIndex, setStepIndex] = useState(0);
  const { opened, stepIndex, children, onClose } = props;
  if (!children || children.length === 0) return null;
  // Le pasamos a los hijos la prop de onClose para que se llame en el cancel de
  // los WizardTab
  const childrenWithProps = Children.map(children, (child) => {
    if (isValidElement(child)) return cloneElement(child, { onClose });
    return child;
  });

  return (
    <Popup opened={opened}>
      <div className="wizard-comp">
        <div className="wizard-comp__header">
          <WizardHeader steps={children.length} current={stepIndex} />
        </div>

        <div className="wizard-comp__content">
          {childrenWithProps[stepIndex]}
        </div>
      </div>
    </Popup>
  );
};

export default Wizard;
