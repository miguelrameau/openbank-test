import React from "react";
import "./index.scss";
import checkIcon from "./check.png";
const WizardHeader = (props) => {
  console.log("props: ", props);
  return <div className="wizard-header-comp">{renderButtons(props)}</div>;
};

const renderButtons = (props) => {
  const { steps, current } = props;
  let stepIcons = [];
  for (let i = 0; i < steps; i++) {
    let classes = `step-icon`;
    let separator = null;
    const isPassed = current > i;
    // Anterior al paso actual

    if (isPassed) {
      classes = `${classes} passed`;
      separator = (
        <div key={`separator ${i}`} className="separator passed"></div>
      );
    }

    // Paso actual
    if (current === i) {
      classes = `${classes} current`;
    }
    // Linea separadora
    if (!separator && i < steps - 1) {
      separator = <div key={`separator ${i}`} className="separator"></div>;
    }

    stepIcons = [
      ...stepIcons,
      <div className={classes} key={i}>
        {!isPassed ? i + 1 : <img src={checkIcon} alt="ok" />}
        {current === i ? <div className="triangle"></div> : null}
      </div>,
      separator,
    ];
  }
  return stepIcons;
};

export default WizardHeader;
