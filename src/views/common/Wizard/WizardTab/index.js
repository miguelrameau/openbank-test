import React from "react";
import i18next from "i18next";

import "../WizardTab/index.scss";
import { Button } from "../../styled/button.styled";

const WizardTab = (props) => {
  const { title, children, onClose, renderButtons, enableNext, onNext } = props;
  return (
    <div className="wizard-tab-comp">
      <div className="wizard-tab-comp__content">
        {title ? (
          <div className="wizard-tab-comp__content-title">
            <h2>{title}</h2>
            <div className="wizard-tab-comp__content-title-under"></div>
          </div>
        ) : null}
        <div className="wizard-tab-comp__content-comp">{children}</div>
      </div>

      {renderButtons !== false ? (
        <div className="wizard-tab-comp__buttons">
          <div
            className="wizard-tab-comp__buttons-cancel"
            onClick={() => (onClose ? onClose() : null)}
          >
            {i18next.t("cancel")}
          </div>
          <Button disabled={!enableNext} onClick={() => onNext()}>
            {i18next.t("next")} {" >"}
          </Button>
        </div>
      ) : null}
    </div>
  );
};

export default WizardTab;
