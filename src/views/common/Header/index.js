import React from "react";
import "./index.scss";
import logo from "./logo.svg";

const Header = () => {
  return (
    <header className="header-comp">
      <div className="logo">
        <img src={logo} alt="OpenBank" />
      </div>
    </header>
  );
};

export default Header;
