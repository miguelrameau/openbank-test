import React, { useState } from "react";
import i18next from "i18next";
import PropTypes from "prop-types";
import eyeEnabled from "./eye-enabled.svg";
import eyeDisabled from "./eye-disabled.svg";

import "../InputPassword/index.scss";
import { Input } from "../styled/input.styled";

const STATE = {
  0: "error",
  1: "warning1",
  2: "warning2",
  3: "ok",
};
const STATE_VALUES = {
  ERROR: 0,
  WARNING1: 1,
  WARNING2: 2,
  OK: 3,
};

const InputPassword = (props) => {
  const { minLength, maxLength, onChange, equalTo } = props;
  const [type, setType] = useState("password");
  const [checkLength, setCheckLength] = useState(0);
  const [checkOneUpperCase, setCheckOneUpperCase] = useState(0);
  const [checkOneNumber, setCheckOneNumber] = useState(0);
  const [currentValue, setCurrentValue] = useState(props.value);

  const toggle = () =>
    type === "password" ? setType("text") : setType("password");

  const onChangeValue = (e) => {
    const val = e.target.value;
    const upperCase = hasUpperCase(val) ? 1 : 0;
    const oneNumber = hasNumber(val) ? 1 : 0;
    const length = hasLength(val) ? 1 : 0;
    setCurrentValue(val);
    setCheckOneUpperCase(upperCase);
    setCheckOneNumber(oneNumber);
    setCheckLength(length);
    console.log(
      "checkStatus() === STATE_VALUES.OK: ",
      checkStatus(val, length, upperCase, oneNumber) === STATE_VALUES.OK
    );
    onChange(
      e,
      checkStatus(val, length, upperCase, oneNumber) === STATE_VALUES.OK
    );
  };

  const checkStatus = (
    value = currentValue,
    length = checkLength,
    upperCase = checkOneUpperCase,
    oneNumber = checkOneNumber
  ) => {
    let status = length + upperCase + oneNumber;
    // en el caso de que tengamos que validar también que la contraseña sea igual a otra,
    // No permitimos que se ponga a OK
    if (status === STATE_VALUES.OK && equalTo && equalTo !== value) {
      status -= STATE_VALUES.WARNING2;
    }
    return status;
  };

  const hasUpperCase = (str) => str && str !== str.toLowerCase();
  const hasNumber = (str) => /\d/.test(str);
  const hasLength = (str) =>
    str && str.length >= minLength && str.length < maxLength;

  const status = checkStatus();
  return (
    <div className="input-password-comp">
      <Input type={type} {...props} onChange={onChangeValue} />
      <span className="input-password-comp__show" onClick={() => toggle()}>
        <img src={type === "password" ? eyeEnabled : eyeDisabled} />
      </span>
      <div className={`input-password-comp__state ${STATE[status]}`}></div>
    </div>
  );
};

InputPassword.defaultProps = {
  checkValidity: true,
  equalTo: null,
  minLength: 8,
  maxLength: 24,
  onChange: () => null,
};
InputPassword.propTypes = {
  checkValidity: PropTypes.bool,
  equalTo: PropTypes.string,
  minLength: PropTypes.number,
  maxLength: PropTypes.number,
  onChange: PropTypes.func,
};

export default InputPassword;
