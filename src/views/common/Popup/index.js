import React from "react";
import PropTypes from "prop-types";

import "./index.scss";
import { Button } from "../styled/button.styled";

const Popup = (props) => {
  const { opened, children, onClose, onConfirm, isModal } = props;
  if (!opened) return null;

  return (
    <div
      role="button"
      tabIndex={0}
      className="popup-comp"
      onClick={() => {
        if (onClose && !isModal) onClose();
      }}
    >
      <div
        className="popup-comp-inside"
        role="button"
        tabIndex={0}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="content">{children}</div>
        {onConfirm || onClose ? (
          <div className="btns">
            {onConfirm ? (
              <Button onClick={() => onConfirm()}>Confirmar</Button>
            ) : null}
            {onClose ? (
              <Button onClick={() => onClose()}> Cerrar</Button>
            ) : null}
          </div>
        ) : null}
      </div>
    </div>
  );
};

Popup.defaultProps = {
  onConfirm: null,
  onClose: null,
};
Popup.propTypes = {
  opened: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired,
  onClose: PropTypes.func,
  onConfirm: PropTypes.func,
  isModal: PropTypes.bool,
};

export default Popup;
