import styled, { css } from "styled-components";

export const Button = styled.button`
  background-color: #002b45;
  color: white;
  padding: 1em;
  padding-left: 2em;
  padding-right: 2em;
  border: none;
  cursor: pointer;
  user-select: none;

  ${(props) =>
    props.disabled &&
    css`
      opacity: 0.5;
      pointer-events: none;
    `};
`;

export default Button;
