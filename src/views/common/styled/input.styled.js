import styled, { css } from "styled-components";

export const Input = styled.input`
  width: 100%;
  padding: 1em;
  border: 0.1em solid #ccd5da;
  outline-color: #ccd5da;
  box-sizing: border-box;

  ${(props) =>
    props.invalid &&
    css`
      border-color: red;
    `};
`;

export default Input;
