import React, { useEffect, useState } from "react";
import i18next from "i18next";

import "../Step2/index.scss";
import InputPassword from "../common/InputPassword";
import { Input } from "../common/styled/input.styled";

const MAX_HINT_LENGTH = 255;
const Step2 = (props) => {
  const { onValidatedChanged } = props;
  const [password, setPassword] = useState({ value: "", isValidated: false });
  const [password2, setPassword2] = useState({ value: "", isValidated: false });
  const [hint, setHint] = useState({ value: "" });

  useEffect(() => {
    onValidatedChanged(password.isValidated && password2.isValidated, {
      password,
      password2,
      hint,
    });
  }, [password, password2]);

  return (
    <div className="step2-comp">
      <p>
        {i18next.t("text_how_it_works")} <br />
        {i18next.t("text_not_able_to_recover")}
        <br />
        {i18next.t("info_password")}
      </p>

      <ul className="step2-comp__inputs">
        <li>
          <h3>{i18next.t("create_your_master_password")}</h3>
          <InputPassword
            className="input"
            value={password.value}
            onChange={(e, isValidated) =>
              setPassword({ isValidated, value: e.target.value })
            }
          />
        </li>
        <li>
          <h3>{i18next.t("repeat_your_master_password")}</h3>
          <InputPassword
            className="input"
            value={password2.value}
            equalTo={password.value}
            onChange={(e, isValidated) =>
              setPassword2({ isValidated, value: e.target.value })
            }
          />
        </li>
      </ul>

      <p>{i18next.t("info_create_hint")}</p>
      <div className="step2-comp__hint">
        <Input
          onChange={(e) => setHint({ value: e.target.value })}
          value={hint.value}
          maxLength={MAX_HINT_LENGTH}
        />
        <div className="step2-comp__hint-length">
          {hint.value.length}/{MAX_HINT_LENGTH}
        </div>
      </div>
    </div>
  );
};

export default Step2;
