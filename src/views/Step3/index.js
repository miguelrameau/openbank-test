import React from "react";
import i18next from "i18next";

import iconOk from "./icon-ok.png";
import iconError from "./icon-error.png";

import "../Step3/index.scss";

const OK_DATA = {
  icon: iconOk,
  title: "password_created_successfully_title",
  text: "password_created_successfully_text",
  btnText: "join",
};

const ERROR_DATA = {
  icon: iconError,
  title: "password_created_error_title",
  text: "password_created_error_text",
  btnText: "back_to_manager",
};
const Step3 = (props) => {
  const { status, onSuccess, onError } = props;

  let data;
  switch (status) {
    case "OK":
      data = OK_DATA;
      break;
    case "ERROR":
      data = ERROR_DATA;
      break;
    default:
      data = null;
  }
  if (status === "LOADING") return <h2>{i18next.t("wait_please")}</h2>;
  if (!data) return null;

  return (
    <div className="step3-comp">
      <div className="step3-comp__info">
        <div className="icon">
          <img src={data.icon} />
        </div>
        <div className="text">
          <h3>{i18next.t(data.title)}</h3>
          <p>{i18next.t(data.text)}</p>
        </div>
      </div>
      <div className="step3-comp__btn">
        <a onClick={() => (status === "OK" ? onSuccess() : onError())}>
          {i18next.t(data.btnText)} {" >"}
        </a>
      </div>
    </div>
  );
};

export default Step3;
