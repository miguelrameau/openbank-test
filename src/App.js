import React, { Component } from "react";
import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import { submitForm } from "./services/api";

import Step1 from "./views/Step1";
import Step2 from "./views/Step2";
import Step3 from "./views/Step3";

import "./App.scss";
import Header from "./views/common/Header";
import Wizard from "./views/common/Wizard";
import WizardTab from "./views/common/Wizard/WizardTab";

// init i18n and inject hardcoded translations
import en from "./locale/en.json";
import es from "./locale/es.json";

class App extends Component {
  state = {
    languageLoaded: false,
    wizardOpened: false,
    step2Validated: false,
    form: null,
    stepIndex: 0,
    registerStatus: "NONE",
  };
  componentWillMount() {
    i18next.use(LanguageDetector).init(
      {
        lng: "es",
        fallbackLng: "es",
        resources: { en, es },
        keySeparator: false,
        debug: true,
        nsSeparator: false,
      },
      (err, t) => {
        if (err) return console.log("something went wrong loading", err);
        this.setState({ languageLoaded: true });
      }
    );
  }

  render() {
    const {
      languageLoaded,
      wizardOpened,
      step2Validated,
      stepIndex,
      registerStatus,
      form,
    } = this.state;

    const onCloseWizard = () => {
      this.setState({
        wizardOpened: false,
        stepIndex: 0,
        step2Validated: false,
        registerStatus: "NONE",
      });
    };

    if (!languageLoaded) return null;

    return (
      <div className="App">
        <Header />

        <Wizard
          key={`key-${wizardOpened}`}
          opened={wizardOpened}
          onClose={onCloseWizard}
          stepIndex={stepIndex}
        >
          <WizardTab
            title={i18next.t("title_create_password")}
            enableNext={true}
            onNext={() => this.setState({ stepIndex: stepIndex + 1 })}
          >
            <Step1 />
          </WizardTab>

          <WizardTab
            title={i18next.t("title_create_password")}
            enableNext={step2Validated}
            onNext={this.onSubmit}
          >
            <Step2
              onValidatedChanged={(step2Validated, form) =>
                this.setState({ step2Validated, form })
              }
            />
          </WizardTab>

          <WizardTab
            title={
              registerStatus === "OK" ? i18next.t("congratulations") : null
            }
            renderButtons={false}
          >
            <Step3
              status={registerStatus}
              onSuccess={onCloseWizard}
              onError={() => this.setState({ stepIndex: 1 })}
            />
          </WizardTab>
        </Wizard>

        <main className="App-content">
          <div className="App-create-account">
            {i18next.t("info_create_account")}{" "}
            <a onClick={() => this.setState({ wizardOpened: true })}>
              {i18next.t("here")}
            </a>
          </div>
          <article>
            <h2>Sobre el test</h2>
            <p>
              Para abrir el "Wizard" que simula la creación de cuenta, hay que
              pulsar el botón <b>"aquí"</b> de la zona de arriba.
            </p>

            <p>
              He decidido usar <b>hooks</b> para el desarrollo de los componentes,
              aunque he mantenido el <b>paradigma</b> de clase en <b>App.js</b> con el
              proposito de demostrar que domino ambas formas de desarrollar
              componentes.
            </p>

            <p>Creé elementos genéricos como <b>Popup, Wizard o InputPassword</b> que pueden ser reutilizables para comportamientos similares.</p>
            
            <p>He usado propTypes en algunos componentes para demostrar que domino su uso.</p>
            <p>He usado la funcionalidad <b>i18next</b> para incluir los literales tal y como mencionaba el enunciado del test. </p>
            <p>He añadido <b>styled-components</b> para demostrar que también me manejo con ellos. </p>
            <p>He decidido usar <b>em</b> como medida para <b>tamaños de letra, padding y margin</b> para que todo se dimensione acorde al <b>font-size heredado</b>.</p>

            <h3>Estructura de carpetas de componentes</h3>
            <p>Cada carpeta de componentes está compuesta por un archivo <b>index.js</b> donde está el código del componente React, un archivo <b>index.scss</b> donde se encuentra el código del diseño de este, recursos como <b>imágenes</b> y <b>subcarpetas de componentes hijos</b> específicos si los tuvieran. Cada uno de ellos tendría la misma estructura de index.js e index.scss.</p>
            <p>El componente principal es <b>App.js</b> que es el único que no está dentro de su carpeta específica.</p>
            <p>En la carpeta de <b>common</b> están los componentes que se podrían usar en otros sitios del proyecto dada su naturaleza <b>reusable</b>.</p>
            <p><b>Step1, Step2 y Step3</b> son los componentes de cada paso del Wizard para la creación de contraseña.</p>
          
            <p>Muchas gracias por revisar el proyecto.</p>
          </article>
        </main>
      </div>
    );
  }

  onSubmit = async () => {
    const { stepIndex, form } = this.state;
    this.setState({
      stepIndex: stepIndex + 1,
      registerStatus: "LOADING",
    });
    try {
      const response = await submitForm(
        form.password.value,
        form.password2.value,
        form.hint.value
      );

      if (response && response.status === 200) {
        this.setState({ registerStatus: "OK" });
      } else {
        this.setState({ registerStatus: "ERROR" });
      }
    } catch (e) {
      this.setState({ registerStatus: "ERROR" });
    }
  };
}

export default App;
